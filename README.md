![Build Status](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-nikolaj-simonsen/badges/main/pipeline.svg)

# ITT3-PCB-Nikolaj-Simonsen

This project serves as an example for 3rd semester students in the [ITT PCB elective 21A](https://21a-itt3-pcb-students-group.gitlab.io/itt3-pcb-nikolaj-simonsen) as well as documentation.
The purpose of the PCB project is to make a general purpose hat for the Raspberry Pi 4.

# Public documentation

[MKDocs](https://www.mkdocs.org/) is used to serve a static webpage containing documentation and information about the project.  
Link to page: [https://21a-itt3-pcb-students-group.gitlab.io/itt3-pcb-nikolaj-simonsen/](https://21a-itt3-pcb-students-group.gitlab.io/itt3-pcb-nikolaj-simonsen/)

# Description

The RPi hat is using the [standard requirements](https://github.com/raspberrypi/hats) for a Raspberry Pi hat, it is only using the 26 standard header pins and it thus not required to add an eeprom.  

The RPi hat is created from this basic block diagram  

![./public-site/docs/img/RPi-gp-hat.png](./public-site/docs/img/RPi-gp-hat.png)

# Resources

* RPi hat outlines [https://github.com/omwah/rpi-hat-outlines](https://github.com/omwah/rpi-hat-outlines)
* RPi hat official [https://github.com/raspberrypi/hats](https://github.com/raspberrypi/hats)
* RPi pinout [https://www.raspberrypi.org/documentation/computers/os.html#gpio-and-the-40-pin-header](https://www.raspberrypi.org/documentation/computers/os.html#gpio-and-the-40-pin-header)

## Project status

Work in progress


