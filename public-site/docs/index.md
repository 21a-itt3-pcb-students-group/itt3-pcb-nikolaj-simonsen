# RPi general purpose hat

This project serves as an example for 3rd semester students in the [ITT PCB elective 21A](https://21a-itt3-pcb-students-group.gitlab.io/itt3-pcb-nikolaj-simonsen) as well as documentation.
The purpose of the PCB project is to make a general purpose hat for the Raspberry Pi 4.

# Description

The RPi hat is using the [standard requirements](https://github.com/raspberrypi/hats) for a Raspberry Pi hat, it is only using the 26 standard header pins and is thus not required to add an eeprom.  

The RPi hat is created from a basic block diagram   

![Blockdiagram RPi general purpose hat](img/RPi-gp-hat.png)  

The schematic and pcb layout are designed in Orcad 17.4.  
Design files are available in a gitlab project located at [https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-nikolaj-simonsen](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-nikolaj-simonsen)

